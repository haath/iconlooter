# Icon looter

![ ](samples/screenshot.png)

## Usage

### Remove watermarks

Remove the watermark from an image.

```sh
iconlooter unmask image.png -o image_unmasked.png
```

Remove the watermark from an image by its URL.

```sh
iconlooter unmask https://cdn3.iconfinder.com/data/icons/food-filled-4/512/Bag-exercise-fitness-gym-512.png
```


### Start server

Start an HTTP server, which serves a single page that prompts the user for an
image URL.

```sh
iconlooter server
```

The webserver can then also be used as an API, by appending image URLs to its
`/icon` endpoint, like in the example below.

```url
http://localhost:8080/icon/cdn3.iconfinder.com/data/icons/food-filled-4/512/Bag-exercise-fitness-gym-512.png
```

