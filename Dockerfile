#################################
#      Build Environment
#################################
FROM golang:1.11.5 AS build-env

# Prepare workdir
RUN mkdir -p $GOPATH/src/gitlab.com/haath/iconlooter
WORKDIR $GOPATH/src/gitlab.com/haath/iconlooter
COPY . $GOPATH/src/gitlab.com/haath/iconlooter

# Install tools
RUN go get -u github.com/golang/dep/cmd/dep
RUN go get -u github.com/gobuffalo/packr/...

RUN dep ensure

RUN packr build --ldflags "-linkmode external -extldflags -static" -i -v -o /build/iconlooter

#################################
#      Runtime Environment
#################################
FROM alpine

RUN apk update && apk add ca-certificates && rm -rf /var/cache/apk/*

WORKDIR /app

COPY --from=build-env /build/iconlooter /app/

CMD ["/app/iconlooter", "server"]
